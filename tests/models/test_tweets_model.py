from app.models.tweets import Tweet


def test_tweets_model():
    tweet = Tweet(text="text")

    assert str(tweet) == "<id: None, text: text>"
    assert tweet.as_dict() == {"id": None, "text": "text"}
