import pytest
import json


@pytest.mark.component
def test_get_all_tweets_empty(app_client):
    response = app_client.get("/tweets")

    assert 200 == response.status_code

    parsed_response = response.json()
    assert parsed_response == []


@pytest.mark.component
def test_create_tweets(app_client):
    response_one = app_client.post(
        "/tweets",
        data=json.dumps(
            {"text": "tweet1"},
        ),
        headers={"content-type": "application/json"},
    )

    response_two = app_client.post(
        "/tweets",
        data=json.dumps(
            {"text": "tweet2"},
        ),
        headers={"content-type": "application/json"},
    )

    assert 201 == response_one.status_code
    assert 201 == response_two.status_code

    parsed_response_one = response_one.json()
    parsed_response_two = response_two.json()
    assert parsed_response_one["id"] == 1
    assert parsed_response_one["text"] == "tweet1"
    assert parsed_response_two["id"] == 2
    assert parsed_response_two["text"] == "tweet2"


@pytest.mark.component
def test_create_tweets_no_text(app_client):
    response = app_client.post(
        "/tweets",
        data=json.dumps(
            {"text": ""},
        ),
        headers={"content-type": "application/json"},
    )

    assert 409 == response.status_code


@pytest.mark.component
def test_get_all_tweets(app_client):
    response = app_client.get("/tweets")

    assert 200 == response.status_code

    parsed_response = response.json()
    assert parsed_response[0]["id"] == 1
    assert parsed_response[0]["text"] == "tweet1"
    assert parsed_response[1]["id"] == 2
    assert parsed_response[1]["text"] == "tweet2"


@pytest.mark.component
def test_get_tweet_by_id(app_client):
    response = app_client.get("/tweets/1")

    assert 200 == response.status_code

    parsed_response = response.json()
    assert parsed_response["id"] == 1
    assert parsed_response["text"] == "tweet1"


@pytest.mark.component
def test_get_tweet_by_id_wrong_id(app_client):
    response = app_client.get("/tweets/10")

    assert 404 == response.status_code


@pytest.mark.component
def test_update_tweet(app_client):
    response = app_client.put(
        "/tweets/1",
        data=json.dumps(
            {"text": "tweet_updated"},
        ),
        headers={"content-type": "application/json"},
    )

    assert 200 == response.status_code

    parsed_response = response.json()
    assert parsed_response["id"] == 1
    assert parsed_response["text"] == "tweet_updated"


@pytest.mark.component
def test_update_tweet_no_message(app_client):
    response = app_client.put(
        "/tweets/2",
        data=json.dumps(
            {"text": ""},
        ),
        headers={"content-type": "application/json"},
    )

    assert 409 == response.status_code

    response = app_client.get("/tweets/2")
    assert 200 == response.status_code

    parsed_response = response.json()
    assert parsed_response["id"] == 2
    assert parsed_response["text"] == "tweet2"


@pytest.mark.component
def test_update_tweet_wrong_id(app_client):
    response = app_client.put(
        "/tweets/10",
        data=json.dumps(
            {"text": "tweet_updated"},
        ),
        headers={"content-type": "application/json"},
    )

    assert 404 == response.status_code


@pytest.mark.component
def test_delete_tweet(app_client):
    response = app_client.delete("/tweets/1")
    assert 204 == response.status_code

    response_get = app_client.get("/tweets/1")
    assert 404 == response_get.status_code


@pytest.mark.component
def test_delete_tweet_wrong_id(app_client):
    response = app_client.delete("/tweets/10")
    assert 404 == response.status_code
