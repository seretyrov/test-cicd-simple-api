# Gitlab CICD test

This is a very simple python flask rest+ api for me to learn gitlab cicd and to deploy 
on both Heroku and AWS Elastic Beanstalk.

## Application overview and structure

This is a very simple 'app' that CRUD kind of tweets, written in python with 
restplus.

Only 4 files are interesting for this CICD test (.gitlab-ci.yml for Gitlab cicd, Procfile 
for Heroku, .elasticbeanstalk/config.yml and .eb-config.sh for AWS EB), the rest is a 
standard restplus app.

### root folder
All important files for this test app are here:
- **.eb-config.sh**: script to configure the aws profile on the target env
- **.gitlab-ci.yml**: contains all stages of the pipeline. Will be read by gitlab 
on each merge request (test), merge (deploy>staging) and tag (deploy>production)
- config.py: app global conf file, loaded at the initialization
- **Procfile**: used by Heroku to launch commands once the build is done
- pytest.ini: pytest global config file
- requirements.txt: all necessary packages to run the app
- wsgi.py: the main app initializer.

### .elasticbeanstalk folder
Contains the main config file to deploy on AWS elastic beanstalk

### app folder
The whole application is in this folder:
- apis: contains the namespace (generates routes and the swagger)
- core: all operations called by each route
- models: where the Tweet object is
- utils: database creation methods
- create_app.py: the method called to link all files and init the app

See [flask rest+ doc](https://flask-restplus.readthedocs.io/en/stable/) for more details.

### images folder
Just a folder to gather images used in this readme

### tests folder
All pytest related files. See [pytest doc](https://docs.pytest.org/en/latest/contents.html) 
for more details

## How to run locally

Clone the repo locally, then open the terminal in your favorite terminal.

Create a venv:
```bash
$ vitualenv venv
OR to force python3
$ virtualenv -p <path_to_python3, ex: usr/bin/python3> venv
```

Once created, activate it:
```bash
$ source venv/bin/activate
```

Install the requirements:
```bash
$ pip install -r requirements.txt
```

Launch pytest to control all is working:
```bash
$ pytest
```

Launch the app locally to test if all is set up:
```bash
$ flask run
```

In any browser, access http://127.0.0.1:5000. If you can play with the swagger,
then it works :)

All is set up!

## Gitlab CICD

### For Heroku

Log in [Heroku](https://www.heroku.com) (or sign in for free) and create a new pipeline.
Give it a name and select create:
![heroku welcome screen with emphasis on create pipeline button](images/heroku_1.png)

On the pipeline dashboard, add one app per environment (staging / production, do not 
foget to change the environment in the ```pipeline``` drop down):
![heroku create app form](images/heroku_2.png)

Once created, change the name of apps in staging and production stages of the 
.gitlab-ci.yml to match your Heroku application names:

```bash
staging-heroku:
  stage: deploy
  script:
  - apt-get update -qy
  - apt-get install -y ruby-dev
  - gem install dpl
  - dpl --provider=heroku --app=<your staging app name> --api-key=$HEROKU_API_KEY
  only:
  - master

production-heroku:
  stage: deploy
  script:
  - apt-get update -qy
  - apt-get install -y ruby-dev
  - gem install dpl
  - dpl --provider=heroku --app=<your production app name> --api-key=$HEROKU_API_KEY
  only:
  - tags
```

Finally, go to your [account settings](https://dashboard.heroku.com/account) 
and scroll down to the ```API Key``` section. Copy the key.
![heroku account settings displaying the API key section](images/heroku_3.png)

Back to Gitlab, in the ```Settings > CI/CD > Environment variables``` of your project, 
add a variable:
```bash
key: HEROKU_API_KEY, value: <paste your API key from Heroku>
```
![gitlab project settings>cicd>variables section, displaying 2 variables created](images/gitlab_1.png)

Create a runner that will do the job for you by following [Gitlab doc](https://docs.gitlab.com/runner/install/linux-repository.html#installing-the-runner)

That's it to deploy to the staging environment! Create a new branch and a PR to see 
the ```test``` stage being performed before allowing the merge.

To track the progress, in your project menu, go to cicd>pipeline and click the pending 
button (passed in the screenshot below).
![gitlab project cicd>pipeline, displaying previous pipelines](images/gitlab_2.png)

Once done, merge your PR. A new pipeline will be triggered, deploying the application 
to the Heroku staging app.

Go back to Heroku dashboard, click on your pipeline name then the staging app name and 
finally on ```open app``` in the top-right side of the screen. 

**Go to prod!**

Now that the staging environment works, lets push the app to production.

Make sure you are on your master branch
```bash
$ git checkout master
$ git pull origin master
```
and add a tag:
```bash
$ git tag -a v1.0 -m ‘Version 1.0’
$ git push origin master --tags
``` 
It will trigger the last part of the .gitlab-ci.yml stage (production>deploy)

Go back to Gitlab>Pipelines to track jobs progress. Once it's done, go to Heroku, 
select your pipeline and your production application, click on ```open app``` and voila :)

References:
- [gitlab cicd](https://docs.gitlab.com/ee/ci/examples/test-and-deploy-python-application-to-heroku.html)
- [tags](https://docs.gitlab.com/ee/university/training/topics/tags.html)

### For AWS

In AWS console, go to Services>IAM>Users and select ```add user```. Name it as you want, 
be sure to check both ```Programmatic access``` and ```AWS management cosole access``` 
options. Add a password and click next.

![AWS console, create user popup](images/aws_1.png)

In the next window, click on ```Create groupe``` and select AWSElasticBeanstalkFullAccess. 
Add a name for the group and confirm.

![AWS console, create user group management popup](images/aws_2.png)

Pass the two next screen until the success admonition. Copy the details (User, Access Key 
ID, Secret access key, console url) and keep them, we'll need them later.

Back to your terminal, install ```aws elastic beanstalk cli```
```bash
$ pip install awsebcli --upgrade --user
```

Initialize your app using the credentials saved previously:
```bash
$ eb init -p python-3.6 <your app name> --region <region of your choice>
```

[liste of AWS endpoints](https://docs.aws.amazon.com/general/latest/gr/rande.html)

Commit and push to your master branch, then create two environments using eb cli:
```bash
eb create <your app name>-dev
eb create <your app name>-prod
```

It can take a few minutes, so grab a coffee/tea :)

Ok, now that your two environments are ready, two things to do:

- change the WSGIPath of BOTH environments:
In ```AWS console>Elastic Beanstalk```, select your app then your first environment.
Go to ```Configuration>Software``` (click on ```Modify```) and update the first parameter 
to:
```bash
WSGIPath: wsgi.py
```
![AWS console, elastic beanstalk environment setup](images/aws_3.png)

**Repeat for the second environment**

- update the conf files:
```bash
#.elasticbeanstalk/config.yml

branch-defaults:
  production:
    environment: <your prod environment name>
    group_suffix: null
  staging:
    environment: <your dev environment name>
    group_suffix: null
global:
  application_name: <your app name>
  branch: null
  default_ec2_keyname: null
  default_platform: python-3.6
  default_region: <the region endpoint you've selected>
  include_git_submodules: true
  instance_profile: null
  platform_name: null
  platform_version: null
  profile: eb-cli
  repository: null
  sc: git
  workspace_type: Application
```

```bash
# .gitlab-ci.yml

...
staging-aws:
  stage: deploy
  image: python:3.6-stretch
  before_script:
  - pip install awsebcli --upgrade --user
  - chmod +x ./.eb-config.sh
  - ./.eb-config.sh
  - git checkout master
  script:
  - /root/.local/bin/eb deploy <your dev environment name>
  only:
  - master

...
production-aws:
  stage: deploy
  image: python:3.6-stretch
  before_script:
  - pip install awsebcli --upgrade --user
  - chmod +x ./.eb-config.sh
  - ./.eb-config.sh
  - git fetch
  - git checkout master
  script:
  - /root/.local/bin/eb deploy <your prod environment name>
  only:
  - tags
```

OK, almost done!

Last thing to do, configure your project cicd variables with AWS credentials. In your 
gitlab project menu, go to ```Settings>cicd>variables``` and add:
```
key: AWS_ACCESS_KEY_ID, value: <your access key id saved previously>
key: AWS_SECRET_ACCESS_KEY, value: <your secret access key saved previously>
```

Save and you are done!

## Final test

Create a new branch and make a change, then add/commit/push. In Gitlab, create a merge 
request and go to ```project>pipeline``` to see the tests running.

![Gitlab pipeline, test passed](images/tests_1.png)

Once passed, merge and go back to the ```pipeline``` section. You will now see an 
additional stage, deploy, to both staging environments (staging-aws and staging-heroku).

![Gitlab pipeline, deploying to staging environment](images/tests_2.png)

Once passed, add a new ```tag``` (see end of Heroku section above). For the last time, go to 
the ```pipeline``` section. You'll see this time that the application is being deployed 
on both prod environments.

![Gitlab pipeline, deploying to production environment](images/tests_3.png)

That's it!

Want to test? Go to ```AWS console>Elastic Beanstalk```. You'll see your two environments.
Click on the -prod one and on the url on top of the screen (might happen to have a blank window, 
just refresh the page). Tadaaa :)

reference:
- [Thomas Himblot's Deploying a Flask application on AWS with Gitlab CI/CD guide](https://medium.com/@thimblot/deploying-a-flask-application-on-aws-with-gitlab-ci-cd-part-1-87392be2129e)