from app.utils import db


class Tweet(db.Model):
    __tablename__ = "tweets"
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(280))

    def __repr__(self):
        return "<id: {}, text: {}>".format(
            self.id,
            self.text,
        )

    def as_dict(self):
        return {
            'id': self.id,
            'text': self.text,
        }
