from flask import Flask
from flask_restplus import Api


def create_app():
    app = Flask(__name__)

    from config import Config
    app.config.from_object(Config)

    from app.utils import database
    database.init_app(app)

    from .apis.tweets import api as tweets
    api = Api()
    api.add_namespace(tweets)
    api.init_app(app)

    app.config['ERROR_404_HELP'] = False
    return app
